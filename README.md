# Example Project

See if you can update this file and correct my spelling mistakes!

   Jon :)

Nik B)

## Creating branches

A good guide has already been documented [here](https://github.com/Kunena/Kunena-Forum/wiki/Create-a-new-branch-with-git-and-manage-branches)

Clone

```bash
git clone git@gitlab.com:nik.sheridan/example_project.git
```

Create branch

```bash
git checkout -b gitlab_pages_branch
```

Push to branch

```bash
git push origin gitlab_pages_branch
```

Show branches

```bash
git branch -a
```

Make some changes and commit

```bash
git push origin gitlab_pages_branch
```

Note how merge request detail is returned:

```bash
nsheridan@srv1:~/code/example_project$ git push origin gitlab_pages_branch
Counting objects: 3, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 568 bytes | 568.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
remote: 
remote: To create a merge request for gitlab_pages_branch, visit:
remote:   https://gitlab.com/nik.sheridan/example_project/merge_requests/new?merge_request%5Bsource_branch%5D=gitlab_pages_branch
remote: 
To gitlab.com:nik.sheridan/example_project.git
   0263b84..407eb70  gitlab_pages_branch -> gitlab_pages_branch
nsheridan@srv1:~/code/example_project$ 
```

### Viewing Branches

Create aliases in '~/.gitconfig':

```bash
[alias]
lg1 = log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all
lg2 = log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all
lg = !"git lg1"
```

Use alias:

```bash
git lg
git lg1
git lg2
```

### Switch back to master after merge

Note like the above, I have branched a fork and therefore I am master and I can merge my own branches.  To merge to the true master I must make a pull request.

```bash
git checkout master
```
